package ru.andreymarkelov.atlas.plugins.amutils.conditions;

import java.util.Map;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

public class HasLinksCondition extends AbstractJiraCondition {
    private final IssueLinkManager issueLinkManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public HasLinksCondition(IssueLinkManager issueLinkManager, JiraAuthenticationContext jiraAuthenticationContext) {
        this.issueLinkManager = issueLinkManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public boolean passesCondition(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        Issue issue = (Issue) transientVars.get("issue");
        return !issueLinkManager.getLinkCollection(issue, jiraAuthenticationContext.getLoggedInUser()).getAllIssues().isEmpty();
    }
}
