package ru.andreymarkelov.atlas.plugins.amutils.workflow.function;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;

public class AssignPostFunctionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory {
    public static final String CUSTOMFIELDS = "fields";
    public static final String CUSTOMFIELD = "cfId";

    private final CustomFieldManager customFieldManager;

    public AssignPostFunctionFactory(CustomFieldManager customFieldManager) {
        this.customFieldManager = customFieldManager;
    }

    private String getCustomFieldName(String cfId) {
        if (cfId == null) {
            return "Not Set";
        }

        CustomField field = customFieldManager.getCustomFieldObject(Long.valueOf(cfId));
        return (field != null) ? field.getName() : "Unknown";
    }

    private Map<Long, String> getCustomFields() {
        Map<Long, String> map = new TreeMap<>();
        for (CustomField field : customFieldManager.getCustomFieldObjects()) {
            map.put(field.getIdAsLong(), field.getName());
        }
        return map;
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> functionParams) {
        Map<String, Object> map = new HashMap<String, Object>();
        if (functionParams != null && functionParams.containsKey(CUSTOMFIELD)) {
            map.put(CUSTOMFIELD, extractSingleParam(functionParams, CUSTOMFIELD));
        } else {
            map.put(CUSTOMFIELD, "");
        }
        return map;
    }

    private String getParam(AbstractDescriptor descriptor, String param) {
        if (!(descriptor instanceof FunctionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

        String value = (String) ((FunctionDescriptor) descriptor).getArgs().get(param);
        return (StringUtils.isNotEmpty(value)) ? value : "";
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(CUSTOMFIELDS, getCustomFields());
        velocityParams.put(CUSTOMFIELD, getParam(descriptor, CUSTOMFIELD));
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put(CUSTOMFIELDS, getCustomFields());
        velocityParams.put(CUSTOMFIELD, "");
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(CUSTOMFIELD, getCustomFieldName(getParam(descriptor, CUSTOMFIELD)));
    }
}
