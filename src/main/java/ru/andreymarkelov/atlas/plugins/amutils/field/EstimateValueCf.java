package ru.andreymarkelov.atlas.plugins.amutils.field;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraDurationUtils;

public class EstimateValueCf extends GenericTextCFType {
    private final JiraDurationUtils jiraDurationUtils;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public EstimateValueCf(
            CustomFieldValuePersister customFieldValuePersister,
            GenericConfigManager genericConfigManager,
            JiraAuthenticationContext jiraAuthenticationContext) {
        super(customFieldValuePersister, genericConfigManager);
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.jiraDurationUtils = ComponentAccessor.getComponentOfType(JiraDurationUtils.class);
    }

    public String getSingularObjectFromString(String value) throws FieldValidationException {
        final String estimateTime = StringUtils.defaultString(value).trim();
        try {
            jiraDurationUtils.parseDuration(estimateTime, jiraAuthenticationContext.getLocale());
        } catch (InvalidDurationException e) {
            throw new FieldValidationException(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.field.estimate.error.format"));
        }
        return estimateTime;
    }

    @Override
    public Map<String, Object> getVelocityParameters(
            Issue issue,
            CustomField field,
            FieldLayoutItem fieldLayoutItem) {
        Map<String, Object> velocityParameters = super.getVelocityParameters(issue, field, fieldLayoutItem);
        return velocityParameters;
    }
}
