package ru.andreymarkelov.atlas.plugins;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import ru.andreymarkelov.atlas.plugins.amutils.util.Utils;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;

public class JqlValidatorFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginValidatorFactory {
    public static final String JQL = "jqlstr";
    public static final String COUNT = "issuecount";
    public static final String ERROR = "error";
    public static final String ERRORVIEW = "errorview";
    public static final String ISCONFIGURED = "isConfigured";
    public static final String EXCLUDEITSELF = "excludeitself";

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> conditionParams) {
        Map<String, Object> map = new HashMap<String, Object>();

        if (conditionParams != null && conditionParams.containsKey(JQL)) {
            map.put(JQL, extractSingleParam(conditionParams, JQL));
        } else {
            map.put(JQL, "");
        }

        if (conditionParams != null && conditionParams.containsKey(COUNT)) {
            String countStr = extractSingleParam(conditionParams, COUNT);
            map.put(COUNT, Utils.isPositiveInteger(countStr) ? Integer.valueOf(countStr) : Integer.valueOf(0));
        } else {
            map.put(COUNT, Integer.valueOf(0));
        }

        if (conditionParams != null && conditionParams.containsKey(ERROR)) {
            map.put(ERROR, extractSingleParam(conditionParams, ERROR));
        }

        if (conditionParams != null && conditionParams.containsKey(EXCLUDEITSELF)) {
            map.put(EXCLUDEITSELF, Boolean.TRUE);
        } else {
            map.put(EXCLUDEITSELF, Boolean.FALSE);
        }

        return map;
    }

    private String getParam(AbstractDescriptor descriptor, String param) {
        if (!(descriptor instanceof ValidatorDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a ValidatorDescriptor.");
        }

        ValidatorDescriptor validatorDescriptor = (ValidatorDescriptor) descriptor;
        String value = (String) validatorDescriptor.getArgs().get(param);

        if (value!=null && value.trim().length() > 0) {
            return value;
        } else  {
            return "";
        }
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(JQL, getParam(descriptor, JQL));
        velocityParams.put(COUNT, getParam(descriptor, COUNT));
        velocityParams.put(ERROR, getParam(descriptor, ERROR));
        velocityParams.put(EXCLUDEITSELF, getParam(descriptor, EXCLUDEITSELF));
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put(JQL, "");
        velocityParams.put(COUNT, Integer.valueOf(0));
        velocityParams.put(ERROR, "");
        velocityParams.put(EXCLUDEITSELF, Boolean.TRUE);
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        String jql = getParam(descriptor, JQL);
        String error = getParam(descriptor, ERROR);

        SearchService searchService = ComponentManager.getComponentInstanceOfType(SearchService.class);
        SearchService.ParseResult parseResult = searchService.parseQuery(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), jql);
        if (parseResult.isValid()) {
            velocityParams.put(JQL, jql);
            velocityParams.put(ISCONFIGURED, Boolean.TRUE);
        } else {
            velocityParams.put(JQL, ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.utils.jqlvalidator.jqloptinvalid"));
            velocityParams.put(ISCONFIGURED, Boolean.FALSE);
        }

        String errorview;
        if (!StringUtils.isNotBlank(error)) {
            error = "ru.andreymarkelov.atlas.plugins.utils.jqlvalidator.jqlunique.error";
            errorview = ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText(error, "XXX-X", "XXXXX");
        } else {
            errorview = String.format(error, "XXX-X: XXXXX");
        }

        velocityParams.put(COUNT, getParam(descriptor, COUNT));
        velocityParams.put(EXCLUDEITSELF, getParam(descriptor, EXCLUDEITSELF));
        velocityParams.put(ERROR, error);
        velocityParams.put(ERRORVIEW, errorview);
    }
}
